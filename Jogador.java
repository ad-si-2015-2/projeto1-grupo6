/**
 * @author gusthavo
 *  classe que faz a configuraçao dos jogadores
 */

import java.io.*;
import java.net.*;

public class Jogador{
	private String nome;
	//indica a porta usada
	private int porta;
	//Endereco IP do servidor, fixo
	private InetAddress ipServidor;
	//Endereco IP do Jogador
	private InetAddress ipJogador;
	//Socket UDP usado para conexao
	private Socket conexao;
	// as cartas do jogador no jogo
	private Cartas[] minhasCartas;
	// soma das cartas
	private int somaCartas;
	//indica se o jogador parou de jogar e estao esperando o resultado final
	private boolean parouDeJogar;
	
	public Jogador(){
		this.minhasCartas = new Cartas[26];
		this.somaCartas = 0;
	}
	
	public Jogador(InetAddress ipServidor, int porta, InetAddress ip) {
		this.ipServidor = ipServidor;
		this.setPorta(porta);
		this.setIpJogador(ip);
		//considerando o baralho com 52 cartas, e o jogo com no minimo 2 jogadores, 
		//entao o maximo de cartas de um jogador e 26
		this.minhasCartas = new Cartas[26];
		this.somaCartas = 0;
	}


	public int getPorta() {
		return porta;
	}


	public void setPorta(int porta) {
		this.porta = porta;
	}

	public Socket getConexao() {
		return conexao;
	}

	public void setConexao(Socket conexao) {
		this.conexao = conexao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public InetAddress getIpJogador() {
		return ipJogador;
	}

	public void setIpJogador(InetAddress ipJogador) {
		this.ipJogador = ipJogador;
	}

	public Cartas[] getMinhasCartas() {
		return minhasCartas;
	}
	
	public Cartas rmCartas(int pos) {
		Cartas cartaMonte = minhasCartas[pos-1];
		minhasCartas[pos-1]=null;
		
		return cartaMonte;
	}
	public boolean verificaJogada(int pos, Cartas cartaMonte){  // verrifica a jogada da thred
		if (( cartaMonte.getNumeroCarta() == minhasCartas[pos-1].getNumeroCarta()) || ( cartaMonte.getNaipesCarta().equals( minhasCartas[pos-1].getNaipesCarta()) ) ) {
			return true;
		}else
		{
			return false;
		}
	}
	
	
	public void addCartas(Cartas carta) {
		
		int i = 0;
		
		for(i = 0; i < minhasCartas.length; i++){
			
			if(minhasCartas[i] == null){
				
				minhasCartas[i] = carta;
				i = minhasCartas.length;
			}	
		}
	}

	public int getSomaCartas() {
		return somaCartas;
	}

	public void setSomaCartas(int somaCartas) {
		this.somaCartas = somaCartas;
	}

	public boolean getParouDeJogar() {
		return parouDeJogar;
	}

	public void setParouDeJogar(boolean parouDeJogar) {
		this.parouDeJogar = parouDeJogar;
	}
	
}