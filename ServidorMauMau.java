import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/**
 * @author Lucas
 * classe servidor que realiza a conex�o das threds e
 * realiza toda a execu��o do jogo
 */

public class ServidorMauMau {
	//Endereco IP do servidor, fixo
	private InetAddress ipServidor;
	//indica a porta usada
	private int porta;
	private ServerSocket servidor;
	// jogadores da partida
	private Jogador[] jogadores;
	// baralho da partida
	private Baralho baralho;
	// campeao da partida
	private Jogador campeao;
	// marcador fim de jogo
	private Boolean partidaFinalizada;
	//numero de jogadores que pararam e esperam por resultado
	private int jogadoresParados;

	private int contMonte =0;
	
	private int mauMau =0;
	
	private Cartas monte = null;
	
	public ServidorMauMau(int porta){

		try {
			this.porta = porta;
			this.servidor = new ServerSocket(this.getPorta());
			this.baralho = new Baralho(1,true);
			this.partidaFinalizada = false;
			this.jogadores = new Jogador[2];
		} catch (IOException e) {
			System.out.println("Erro ao iniciar o servidor");
		}

	}

	public ServidorMauMau(Jogador[] jogadores, int porta) throws IOException {
		this.jogadores = jogadores;
		this.porta = porta;
		this.servidor = new ServerSocket(this.getPorta());

		if(jogadores.length < 2){
			//valida��ao numero de jogadores

			// A CRIAR 
		}
		else{		
			this.baralho = new Baralho(1,true);
			this.partidaFinalizada = false;
			iniciar();
		}

	}

	public void iniciar(){

		while(true){

			System.out.println("Aguardando conexoes na porta " + porta);

			try {

				if (jogadores[0] == null) {

					jogadores[0] = new Jogador();

					jogadores[0].setConexao(servidor.accept());
					enviarMsg(jogadores[0].getConexao(), "--------------ATENCAO---------------\n"
														+"VOCE SERA O PRIMEIRO JOGADOR A JOGAR\n"
														+"------------------------------------");
					enviarMsg(jogadores[0].getConexao(), "Conexao aceita, bem vindo ao jogo!\nQual o nick nome?");
					jogadores[0].setNome(lerMsg(jogadores[0].getConexao()));
					enviarMsg(jogadores[0].getConexao(), "Aguardando demais jogadores, aguarde " + jogadores[0].getNome());
				}

				else if(jogadores[1] == null){

					jogadores[1] = new Jogador();

					jogadores[1].setConexao(servidor.accept());
					
					enviarMsg(jogadores[1].getConexao(), "Conexao aceita, bem vindo ao jogo!\nQual o seu nome?");
					jogadores[1].setNome(lerMsg(jogadores[1].getConexao()));

					//Comunica a todos que o jogo vai comecar!
					enviarMsg("O jogo vai comecar em...");
					Thread.sleep(1000);
					enviarMsg("3");
					Thread.sleep(1000);
					enviarMsg("2");
					Thread.sleep(1000);
					enviarMsg("1");
					Thread.sleep(1000);
					this.distribuiCartas();	
					
					fecharConexaoJogadores();
					
				}

				else{
					Socket s = servidor.accept();
					enviarMsg(s, "Conexao nao aceita, ja existem jogadores suficientes! Por favor aguarde a proxima rodada");
					s.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Erro ao tentar conexcao com o Jogador");
			}
		}
	}

	//metodo que distribui cartas para os jogadores e verifica o ganhador
	public void distribuiCartas(){

		//contador de cartas/rodadas
		int k = 0;

		while(!partidaFinalizada && k <= 51){

			for(int i = 0; i < this.jogadores.length; i++){

				// a primeira rodada, apenas distribui cinco cartas a cada jogador
				if(k < jogadores.length){
					this.jogadores[i].addCartas(baralho.darProximaCarta());
					this.jogadores[i].addCartas(baralho.darProximaCarta());
					this.jogadores[i].addCartas(baralho.darProximaCarta());
					this.jogadores[i].addCartas(baralho.darProximaCarta());
					this.jogadores[i].addCartas(baralho.darProximaCarta());
					
					
					k++;
					
					
				}
				
				//Se todos decidiram parar para receber o resultado no final do jogo, então o campeao e informado
				else if(this.jogadoresParados == jogadores.length){
					
					campeao = new Jogador();
					campeao.setSomaCartas(0);
					
					for(Jogador j : jogadores){	
						campeao =  campeao.getSomaCartas() <= j.getSomaCartas() && j.getSomaCartas() <= 21 ? j : campeao; 						
					}
					
					this.partidaFinalizada = true;
					i = this.jogadores.length;
					informaCampeao();
					
				}

				else{
					int count =0;// a partir da segunda rodada, conferir se algum jogador acabou com todas as cartas
					for(int j = 0; j < jogadores.length; j++){
					//	System.out.println ("Satanas e voce? :\n");
						for(Cartas carta : jogadores[j].getMinhasCartas()){
							//System.out.println ("Ja parou jessica ? :\n");
							if(carta != null){
							//	System.out.println ("Opaaaa temos uma nula safadenho\n");
								count++;
							}
						}	
					//	System.out.println ("Quanto e o contador? " + count);
					if(count == 0){
						campeao = jogadores[j];
						partidaFinalizada = true;
					
						}
							j = jogadores.length;
						}
					}
						
						//verifica se algum jogador estourou (passou de 21), nesse caso ele ja e um perdedor (esta fora da partida)
						//else if(this.jogadores[j].getSomaCartas() > 21){
						//	enviarMsg(jogadores[j].getConexao(), "\n\n" + jogadores[j].getNome() + ", voce estourou a quantidade de pontos, voce esta fora!!!");
					//	campeao = j == 0 ? jogadores[1] : jogadores[0];
						//	this.partidaFinalizada = true;
						//	j = this.jogadores.length;
						//}
					
System.out.println ("A PARTIDA FOI FINALIZADA??????? " + partidaFinalizada);
				 	if((partidaFinalizada == false) && !jogadores[i].getParouDeJogar()){

						menu(jogadores[i]);
						enviarMsg(jogadores[i].getConexao(), "\n\nAguarde enquanto os outros jogadores fazem sua jogada");
					}
					//Mostra o vencedor do jogo
					else {
						i = this.jogadores.length;
						informaCampeao();
					}
						}
			}
		}

	
	
	/**
	 * Metodo que envia mensagens informando o vencedor do jogo.
	 * */
	public void informaCampeao(){
		
		this.enviarMsg(campeao.getConexao(), "Parabens " + campeao.getNome() + ", voce e o vencedor!\n");
		this.enviarMsg("O jogador(a) " + campeao.getNome() + " ganhou o jogo!!! \n");
		
	}
	
	public void fecharConexaoJogadores(){
		
		enviarMsg("Sua conexao sera fechada!!!");
		
		for(Jogador j : jogadores){
			try {
				Thread.sleep(1000);
				j.getConexao().close();
				j = null;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println("Erro ao fechar conexao");
			}
		}
		
	}

	public void menu(Jogador jogador){

		boolean loop = true;
	
		String opcoes = "Digite uma das opcoes!!\r\n\r\n"
				+ "1-Pegar Carta\r\n"
				+ "2-Desistir, voce deve aguardar ate o jogo terminar ou todos desistirem\r\n"
				+ "3-Descartar\r\n"
				+ "4-Listar minhas cartas\r\n\r\n"
				+ "5 - MAU MAU\r\n"
				+ "Digite o numero da opcao escolhida: ";


		
		while (loop) {


			
			
			
			
			
			if ( contMonte == 0){
				monte = baralho.darProximaCarta();
			}else{
				
			}
			contMonte++;			
			
			enviarMsg(jogador.getConexao(), "A carta do monte e: " + monte );
			
			enviarMsg(jogador.getConexao(), opcoes);
			String ops = lerMsg(jogador.getConexao());
			int op = Integer.parseInt(ops);

			switch (op) {
			case 1:
				this.addCartas(jogador);
				loop = false;
				break;
			case 2:
				jogador.setParouDeJogar(true);
				this.jogadoresParados++;
				loop = false;
				break;
			case 3:
				
				verificaMauMau(jogador, mauMau);
				mauMau =0;
				enviarMsg(jogador.getConexao(), "Escolha a opcao correspondente a carta que sera descartada: " + "\n\n");
				enviarMsg(jogador.getConexao(), listarCartas(jogador) + "\n\n");
				int op2 = Integer.parseInt(lerMsg(jogador.getConexao()));
				
				if (jogador.verificaJogada(op2, monte) == true){
					 monte = rmCartas(jogador, op2);
					 loop = false;
				}else{
				enviarMsg(jogador.getConexao(), "\nVoce nao pode fazer esta jogada, atente-se para as regras\n");
				}
				
				
				
				break;
			case 4:
				enviarMsg(jogador.getConexao(), listarCartas(jogador) + "\n\n");
				break;
			case 5:
				mauMau =1;
				verificaMauMau(jogador, mauMau);

				
				
				//loop = false;
				//enviarMsg(jogador.getConexao(), listarCartas(jogador) + "\n\n");
				break;
				
			default:
				enviarMsg(jogador.getConexao(), "Opcao invalida, digite uma correta" + "\n\n");
				break;
			}
		}
	}
	
	public void addCartas(Jogador jogador){
		
		jogador.addCartas(baralho.darProximaCarta());
		enviarMsg(jogador.getConexao(), listarCartas(jogador));
		
		
	}
	
public Cartas rmCartas(Jogador jogador, int pos){
		
	int count =0;	
	Cartas monte = null;
	for(Cartas carta : jogador.getMinhasCartas()){
		count++;
		if(count == pos){
		 monte = jogador.rmCartas(pos);
		}
		
	}
		enviarMsg(jogador.getConexao(), listarCartas(jogador));
		
		return monte;
	}
	
public boolean verificaJogada(Jogador jogador, int pos, Cartas monte){
	boolean resp = false;
	int count =0;	
	for(Cartas carta : jogador.getMinhasCartas()){
		count++;
		if(count == pos){
		 resp = jogador.verificaJogada(pos,monte);
		}
		
	}
	
	
	
	return resp;
}



	public void verificaMauMau (Jogador player, int Mau){
		//VERIFICA SE JOGADOR PEDIU MAU MAU OU ESQUECEU DE PEDIR	
		int count =0;	
		for(Cartas carta : player.getMinhasCartas()){
			
			if(carta != null){
				count++;
			}
		}
		if ((Mau == 0 ) && ( count == 1)){ //Esqueceu de pedir
			enviarMsg("Voce esqueceu de pedir MAU MAU, receba 5 cartas.");
			player.addCartas(baralho.darProximaCarta());
			player.addCartas(baralho.darProximaCarta());
			player.addCartas(baralho.darProximaCarta());
			player.addCartas(baralho.darProximaCarta());
			player.addCartas(baralho.darProximaCarta());
			Mau =0; 
			
		}else if ((Mau == 1 ) && ( count != 1)){ // Pediu errado
			enviarMsg("Voce pediu MAU MAU na hora errada!, receba 5 cartas.");
			player.addCartas(baralho.darProximaCarta());
			player.addCartas(baralho.darProximaCarta());
			player.addCartas(baralho.darProximaCarta());
			player.addCartas(baralho.darProximaCarta());
			player.addCartas(baralho.darProximaCarta());
			Mau = 1; 
		}else{
			// Pediu certo ou n�o pediu mau mau na hora certa
		}
	
	}


	public String listarCartas(Jogador jogador){
		
		String mensagem = "\nLista de cartas\n";
		int count = 0;
		for(Cartas carta : jogador.getMinhasCartas()){
			count++;
			if(carta != null)
				
				mensagem += String.valueOf(count) + " - " + carta.toString() +  "\n";
			
		}
		
		return mensagem;
		
	}

	public String lerMsg(Socket clienteSocket){

		try {

			BufferedReader reader = new BufferedReader(new InputStreamReader(clienteSocket.getInputStream()));

			return reader.readLine();
		} catch (Exception e) {
			System.out.println("Erro na leitura da mensagem");
			return null;
		}
	}

	public Boolean enviarMsg(Socket clienteSocket, String msg){

		String msgEnvio = "\n";
		msgEnvio += msg;
		DataOutputStream saida;

		try {

			saida = new DataOutputStream(clienteSocket.getOutputStream());
			saida.writeBytes(msgEnvio);

		} catch (IOException e) {
			return false;
		}
		System.out.println(msgEnvio);

		return true;
	}

	/**
	 * Metodo que envia a mensagem para todos os jogadores
	 */
	public Boolean enviarMsg(String msg){

		String msgEnvio = "\nMensagem para todos os jogadores: ";
		msgEnvio += msg;
		DataOutputStream saida = null;

		try {

			for(Jogador jogador : jogadores){

				saida = new DataOutputStream(jogador.getConexao().getOutputStream());
				saida.writeBytes(msgEnvio);
			}

		} catch (IOException e) {
			return false;
		}
		System.out.println(msgEnvio);

		return true;
	}

	public int getPorta() {
		return porta;
	}

	public void setPorta(int porta) {
		this.porta = porta;
	}

	public ServerSocket getServidor() {
		return servidor;
	}

	public void setServidor(ServerSocket servidor) {
		this.servidor = servidor;
	}

	public int getJogadoresParados() {
		return jogadoresParados;
	}

	public void setJogadoresParados(int jogadoresParados) {
		this.jogadoresParados = jogadoresParados;
	}

}
