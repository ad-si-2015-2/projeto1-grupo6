import java.util.Random;

/**
 * Implementação do  Baralho das Cartas.
 *
 * @author Guilherme
 * 
 *
 */
public class Baralho {
	/**
	 *  array das Cartas 
	 */
	private Cartas[] myCartas;
	/**
	 * O numero de cartas no baralho.
	 */
	private int numCartas;


	 
	public Baralho() {
		
		this(1, false);
	}

	/**
	 * Construtor para definir o numero do baralho ( 52 Cartas ) 
	 *
	 * @param numBaralhos
	 *            
	 * @param embaralhar
	 *            
	 */
	public Baralho(int numBaralhos, boolean embaralhar) {
		this.numCartas = numBaralhos * 52;
		this.myCartas = new Cartas[this.numCartas];
		
		int c = 0;
		
		for (int d = 0; d < numBaralhos; d++) {
			// para cada naipe
			for (int s = 0; s < 4; s++) {
				// para cada número
				for (int n = 1; n <= 13; n++) {
					// adicionar um novo Carta ao Baralho
					this.myCartas[c] = new Cartas(Naipes.values()[s], n);
					// incrementar Cartas index
					c++;
				}
			}
		}
		// embaralhar, se nescessario
		if (embaralhar) {
			this.embaralhar();
		}
	}

	/**
	 * embaralhar  por random.
	 */
	public void embaralhar() {
		
		Random random = new Random();
		
		Cartas temp;
		int j;
		for (int i = 0; i < this.numCartas; i++) {
			
			j = random.nextInt(this.numCartas);
			
			temp = this.myCartas[i];
			this.myCartas[i] = this.myCartas[j];
			this.myCartas[j] = temp;
		}
	}

	/**
	 * Distribuir a próxima Carta a partir do topo do Baralho .
	 *
	 */
	public Cartas darProximaCarta() {
		// pegar topo Cartas
		Cartas topo = this.myCartas[0];
		// deslocar todos os outros Cartas para a esquerda
		for (int c = 1; c < this.numCartas; c++) {
			this.myCartas[c - 1] = this.myCartas[c];
		}
		this.myCartas[this.numCartas - 1] = null;
		// decrement number of Cartas
		this.numCartas--;
		// return topo 
		return topo;
	}

	/**
	 * Print do topo,  do Baralho.
	 *
	 * @param numToPrint
	 *            o número de Cartas a partir do topo do Baralho para imprimir
	 */
	public void printBaralho(int numDoPrint) {
		// print do topo Cartas
		for (int c = 0; c < numDoPrint; c++) {
			System.out.printf("% 3d/%d) %s\n", c + 1, this.numCartas,
					this.myCartas[c].toString());
		}
		System.out.printf(" [%d others]\n\n", this.numCartas - numDoPrint);
	}
}