public class Cartas {

	private Naipes naipes;
	
	private int numeroCarta;
	private String naipeCarta;

	/**
	 * Carta Construtor.
	 *@author Guilherme de almeida
	 * @param naipes
	 *            O naipe das cartas Paus, Ouros, Espadas, Copas
	 * @param numeroCartas
	 *            o numero das cartas,  Valete-Rei : 11-13 e Ace : 1
	 */
	public Cartas(Naipes n, int c) {
		this.naipes = n;
		this.setNumeroCarta(c);
	}

	
	public String toString() {
		String numStr = "Error";
		switch (this.numeroCarta) {
		case 2:
			numStr = "Dois";
			break;
		case 3:
			numStr = "Tres";
			break;
		case 4:
			numStr = "Quatro";
			break;
		case 5:
			numStr = "Cinco";
			break;
		case 6:
			numStr = "Seis";
			break;
		case 7:
			numStr = "Sete";
			break;
		case 8:
			numStr = "Oito";
			break;
		case 9:
			numStr = "Nove";
			break;
		case 10:
			numStr = "Dez";
			break;
		case 11:
			numStr = "Valete";
			break;
		case 12:
			numStr = "Dama";
			break;
		case 13:
			numStr = "Rei";
			break;
		case 1:
			numStr = "As";
			break;
		}
		return numStr + " de " + naipes.toString();
	}

	public int getNumeroCarta() {
		return numeroCarta;
	}

	public String getNaipesCarta() {
		
		naipeCarta = toString().substring(toString().indexOf("de") + 1, toString().length());
		
		return naipeCarta;
	}
	
	public void setNumeroCarta(int numeroCarta) {
		this.numeroCarta = numeroCarta;
	}

}
